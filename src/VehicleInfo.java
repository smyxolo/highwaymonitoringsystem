import java.time.LocalDateTime;

public class VehicleInfo {
    private String numberPlate;
    private CarType TYPE;
    private LocalDateTime entryTime = LocalDateTime.now();

    public VehicleInfo(String registrationNumber, CarType TYPE) {
        this.numberPlate = registrationNumber;
        this.TYPE = TYPE;
    }

    public CarType getTYPE() {
        return TYPE;
    }

    public LocalDateTime getEntryTime() {
        return entryTime;
    }
}
