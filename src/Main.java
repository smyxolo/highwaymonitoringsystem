import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner skan = new Scanner(System.in);
        String inputLine = "";
        boolean isWorking = true;
        Highway a1 = new Highway();
        printOptions();
        while (isWorking){
            inputLine = skan.nextLine();
            if(inputLine.contains("quit")){
                isWorking = false;
            }
            else try {
                String[] inputArguments = inputLine.split(" ");
                String command = inputArguments[0];
                if(command.equals("enter")){
                    String numberPlate = inputArguments[2];
                    CarType carType = CarType.valueOf(inputArguments[1].toUpperCase());
                    a1.vehicleEntry(numberPlate, carType);
                    System.out.println("Vehicle entry recorded.");
                }
                else if(command.equals("leave")){
                    String numberPlate = inputArguments[1];
                    a1.vehicleLeave(numberPlate);
                }
                else if(command.equals("check")){
                    String numberPlate = inputArguments[1];
                    a1.printVehicleInfo(numberPlate);
                }
                else if(command.equals("options")){
                    printOptions();
                }
            }catch (IllegalArgumentException iae){
                System.out.println("Wrong car type given.");
            }catch (DuplicateRecordException dre){
                System.out.println("This numberPlate is registered present on the highway. Please call the Police.");
            }catch (NoRecordException nre){
                System.out.println("This numberPlate is not present in highway database. Please contact your supervisor");
            }catch (ArrayIndexOutOfBoundsException aioobe){
                System.out.println("Not enough information. Please try again or type 'options' to check what's available.");
            }
        }
    }
    private static void printOptions(){
        System.out.println("Options: ");
        System.out.println("-> enter + type[CAR/MOTORCYCLE/TRUCK] + numberPlate");
        System.out.println("-> check + numberPlate");
        System.out.println("-> leave + numberPlate");
        System.out.println("-> options (to show these options again)");
        System.out.println("-> quit (to terminate program)");
    }
}
