import java.time.Duration;
import java.time.LocalDateTime;
import java.util.HashMap;

public class Highway {
    private HashMap<String, VehicleInfo> vehicleMapByRegistraion = new HashMap<>();

    public void vehicleEntry (String registrationNumber, CarType TYPE){
        if(!vehicleMapByRegistraion.containsKey(registrationNumber)) {
            vehicleMapByRegistraion.put(registrationNumber, new VehicleInfo(registrationNumber, TYPE));
        }
        else throw new DuplicateRecordException();
    }

    public VehicleInfo searchVehicle (String numberPlate){
        if(vehicleMapByRegistraion.containsKey(numberPlate)){
            return vehicleMapByRegistraion.get(numberPlate);
        }
        else throw new NoRecordException();
    }

    public void printVehicleInfo(String numberPlate){
        VehicleInfo vehicle = searchVehicle(numberPlate);
        Duration duration = Duration.between(vehicle.getEntryTime(), LocalDateTime.now());
        System.out.println(vehicle.getTYPE() + " registration number " + numberPlate + ":");
        System.out.println("on the highway since " + vehicle.getEntryTime());
        System.out.println("Current travel time: " + duration.toMillis() + " milliseconds");
    }

    public double getQuote(CarType TYPE, Duration duration){
        double quote = 0;
        switch (TYPE){
            case MOTORCYCLE: quote = 2.40 * duration.toMillis() / 1000;
            break;
            case CAR: quote = 4.80 * duration.toMillis() / 1000;
            break;
            case TRUCK: quote = 8.95 * duration.toMillis() / 1000;
            break;
        }
        return quote;
    }

    public void vehicleLeave(String numberPlate){
            LocalDateTime exitTime = LocalDateTime.now();
            VehicleInfo leavingVehicle = searchVehicle(numberPlate);
            System.out.println("Vehicle " + numberPlate + " is leaving at " + exitTime);
            Duration duration = Duration.between(leavingVehicle.getEntryTime(), exitTime);
            System.out.println("Time spent on the highway: " + duration.toMillis() + " milliseconds.");
            System.out.println("Quote to pay: " + getQuote(leavingVehicle.getTYPE(), duration) + " PLN");
            vehicleMapByRegistraion.remove(numberPlate);
    }

}
